====================
Python GIS exercises
====================

Code examples written by myself (sometimes a little bit modified) from the book
`Python Geospatial Development`_ from Erik Westra.

.. _`Python Geospatial Development`: https://www.packtpub.com/application-development/python-geospatial-development-third-edition
