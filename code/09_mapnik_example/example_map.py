import argparse

import mapnik


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser(
    description="Example: python example_map.py shapefile/border.shp"
)
parser.add_argument('shapefile', help='The path to the shapefile to use.')
parser.add_argument(
    'min_latitude',
    help='Minimum latitude to show.',
    type=float,
    nargs='?',
    default=-35.0
)
parser.add_argument(
    'max_latitude',
    help='Maximum latitude to show.',
    type=float,
    nargs='?',
    default=35.0
)
parser.add_argument(
    'min_longitude',
    help='Minimum longitude to show.',
    type=float,
    nargs='?',
    default=-12
)
parser.add_argument(
    'max_longitude',
    help='Maximum longitude to show.',
    type=float,
    nargs='?',
    default=50
)
args = parser.parse_args()


MIN_LATITUDE = args.min_latitude
MAX_LATITUDE = args.max_latitude
MIN_LONGITUDE = args.min_longitude
MAX_LONGITUDE = args.max_longitude
MAP_WIDTH = 700
MAP_HEIGHT = 800

polygon_style = mapnik.Style()

# Draw Angola in dark red and the other countries in dark green.
rule = mapnik.Rule()
rule.filter = mapnik.Filter("[NAME] = 'Angola'")
symbol = mapnik.PolygonSymbolizer()
symbol.fill = mapnik.Color('#604040')
rule.symbols.append(symbol)

polygon_style.rules.append(rule)

rule = mapnik.Rule()
rule.filter = mapnik.Filter("[NAME] != 'Angola'")
symbol = mapnik.PolygonSymbolizer()
symbol.fill = mapnik.Color('#406040')
rule.symbols.append(symbol)

polygon_style.rules.append(rule)

# Define additional rule to draw polygon outlines.
rule = mapnik.Rule()
symbol = mapnik.LineSymbolizer()
symbol.fill = mapnik.Color('#000000')
# symbol. 0.1
rule.symbols.append(symbol)

polygon_style.rules.append(rule)

# Define style for the "Labels" layer.
label_style = mapnik.Style()

# rule = mapnik.Rule()
# symbol = mapnik.TextSymbolizer(
#     mapnik.Expression("[NAME]"),
#     "DejaVu Sans Book", 12,
#     mapnik.Color('#000000')
# )
# rule.symbols.append(symbol)
#
# label_style.rules.append(rule)

# Now define map's layers.
datasource = mapnik.Shapefile(file=args.shapefile)
polygon_layer = mapnik.Layer('Polygons')
polygon_layer.datasource = datasource
polygon_layer.styles.append('PolygonStyle')

label_layer = mapnik.Layer('Labels')
label_layer.datasource = datasource
label_layer.styles.append('LabelStyle')

my_map = mapnik.Map(MAP_WIDTH, MAP_HEIGHT, '+proj=longlat +datum=WGS84')
my_map.background = mapnik.Color('#8080a0')
my_map.append_style('PolygonStyle', polygon_style)
my_map.append_style('LabelStyle', label_style)
my_map.layers.append(polygon_layer)
my_map.layers.append(label_layer)

# Zoom in the desired area of the world and render the map into an image file.
my_map.zoom_to_box(
    mapnik.Box2d(
        MIN_LONGITUDE,
        MIN_LATITUDE,
        MAX_LONGITUDE,
        MAX_LATITUDE
    )
)
mapnik.render_to_file(my_map, 'map.png')
