"""
Analyze the elevations of New Zealand territory by using a digital elevation map
(DEM).

The DEM data file can be downloaded as a compressed file from here:
    https://www.ngdc.noaa.gov/mgg/topo/DATATILES/elev/l10g.zip

The file needs to be georeferenced onto the Earth's surface so that you can
match up each elevation value with its position on the Earth. For this, there is
an associated header file for the premade tiles:
    https://www.ngdc.noaa.gov/mgg/topo/elev/esri/hdr/l10g.hdr
"""
import struct

from osgeo import gdal, gdalconst


# Load the DEM file data.
dataset = gdal.Open('l10g')
band = dataset.GetRasterBand(1)

# Stick to New Zealand territory only, dismiss other countries of the DEM.

# Start by obtaining dataset coordinates transformation.
transform = dataset.GetGeoTransform()

# We want to take the latitude/longitude and calculate associated X and Y.
transform_inverse = gdal.InvGeoTransform(transform)

# We now have the inverse transformation, it is possible to convert from a
# latitude and longitude to X and Y.
# New Zealand frame coordinates are:
# - Min. longitude: 165 degrees.
# - Min. latitude: -48 degreees.
# - Max. longitude: 179 degrees.
# - Max. latitude: -33 degrees.
x_max, y_max = gdal.ApplyGeoTransform(transform_inverse, 179, -33)
x_min, y_min = gdal.ApplyGeoTransform(transform_inverse, 165, -48)

# We can finally identify minimum and maximum coordinates that cover NZ area.
minX = int(min(x_max, x_min))
maxX = int(max(x_max, x_min))
minY = int(min(y_max, y_min))
maxY = int(max(y_max, y_min))

# Now use GDAL to read the individual height values.
width = (maxX - minX) + 1
str_format = '<' + ('h' * width)

histogram = {}  # Maps elevation to number of ocurrences.

# Every tile contains a value of -500 for oceans, with no values between
# -500 and the minimum value for land noted there.
# GDAL library includes a 'no data value' to handle this situation.
no_data = band.GetNoDataValue()

for y in range(minY, maxY + 1):
    scanline = band.ReadRaster(minX, y, width, 1, width, 1, gdalconst.GDT_Int16)
    values = struct.unpack(str_format, scanline)

    for value in values:
        try:
            assert value != no_data
            histogram[value] += 1
        except KeyError:
            histogram[value] = 1
        except AssertionError:
            # The value corresponds to an ocean location. Dismiss it.
            pass

    for height in sorted(histogram.keys()):
        print(height, histogram[height])
