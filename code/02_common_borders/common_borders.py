"""
This script makes use of the World Borders Dataset to obtain polygons defining
the borders of Thailand and Myanmar. Then, it transfers these polygons into
Shapely and uses Shapely's capabilities to calculate the common border between
these two countries, saving the result into another shapefile.
"""
import argparse
import os
import shutil

import shapely.wkt
from osgeo import ogr, osr


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser()
parser.add_argument('shapefile', help='The path to the shapefile to use.')
args = parser.parse_args()

shapefile = ogr.Open(args.shapefile)
layer = shapefile.GetLayer(0)

thailand = None
myanmar = None

# Load the country outlines in Shapely.
for i in range(layer.GetFeatureCount()):
    feature = layer.GetFeature(i)

    if feature.GetField('ISO2') == 'TH':
        geometry = feature.GetGeometryRef()
        thailand = shapely.wkt.loads(geometry.ExportToWkt())
    elif feature.GetField('ISO2') == 'MM':
        geometry = feature.GetGeometryRef()
        myanmar = shapely.wkt.loads(geometry.ExportToWkt())

# Use Shapely's computational geometry capabilities to calculate the common
# border into a new shapefile.
common_border = thailand.intersection(myanmar)

# Result is a `LineString`, or a `MultiLineString` in case the border is broken
# up into more than one part.
if os.path.exists('common-border'):
    shutil.rmtree('common-border')

os.mkdir('common-border')

spatial_reference = osr.SpatialReference()
spatial_reference.SetWellKnownGeogCS('WGS84')

driver = ogr.GetDriverByName('ESRI Shapefile')
dst_path = os.path.join('common-border', 'border.shp')
dst_file = driver.CreateDataSource(dst_path)
dst_layer = dst_file.CreateLayer('layer', spatial_reference)

wkt = shapely.wkt.dumps(common_border)

feature = ogr.Feature(dst_layer.GetLayerDefn())
feature.SetGeometry(ogr.CreateGeometryFromWkt(wkt))
dst_layer.CreateFeature(feature)
