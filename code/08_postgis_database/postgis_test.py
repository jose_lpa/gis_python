"""
This script will first create a table on a given PostgreSQL/PostGIS database to
store the data. After that, it will load a given shapefile features into the
table and then, by using the latitude, longitude and radius provided by user,
it will return a list of names of the countries that are inside a radius of a
given kilometres from the latitude/longitude point.
"""
import argparse

import psycopg2
from osgeo import ogr


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser(
    description="Example: python postgis_test.py shapefile/border.shp "
                "8.542 47.377 500000"
)
parser.add_argument('shapefile', help='The path to the shapefile to use.')
parser.add_argument('longitude', help='Initial position longitude.')
parser.add_argument('latitude', help='Initial position latitude.')
parser.add_argument('radius', help='Radius from initial coordinates.')
args = parser.parse_args()

shapefile = ogr.Open(args.shapefile)
layer = shapefile.GetLayer(0)

# Prepare the database table.
connection = psycopg2.connect(database='gis_python', user='jose')
cursor = connection.cursor()

cursor.execute('DROP TABLE IF EXISTS borders')
cursor.execute(
    'CREATE TABLE borders ('
    'id SERIAL PRIMARY KEY,'
    'name VARCHAR NOT NULL,'
    'iso_code VARCHAR NOT NULL,'
    'outline GEOGRAPHY)'
)
cursor.execute('CREATE INDEX border_index ON borders USING GIST(outline)')
connection.commit()

# Fill up the database with the world borders dataset.
for i in range(layer.GetFeatureCount()):
    feature = layer.GetFeature(i)
    name = feature.GetField('NAME')
    iso_code = feature.GetField('ISO3')
    geometry = feature.GetGeometryRef()
    wkt = geometry.ExportToWkt()

    cursor.execute(
        'INSERT INTO borders (name, iso_code, outline) '
        'VALUES (%s, %s, ST_GeogFromText(%s))', (name, iso_code, wkt)
    )

connection.commit()

# Make the query!
cursor.execute(
    'SELECT name FROM borders WHERE ST_DWithin('
    'ST_MakePoint(%s, %s), outline, %s)',
    (args.longitude, args.latitude, args.radius)
)

print(
    'Countries that are within {radius} kilometers from '
    '{latitude},{longitude}:'.format(
        longitude=args.longitude,
        latitude=args.latitude,
        radius=int(args.radius)/1000
    )
)
for row in cursor:
    print(row[0])
