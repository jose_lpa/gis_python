"""
This program will take a shapefile containing all core-based statistical areas
in the United States, then a GNIS place name data file, compare them both and
identify parks in urban areas of the California State.
"""
import argparse

import shapely.geometry
import shapely.wkt
from osgeo import ogr


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser()
parser.add_argument('shapefile', help='The path to the GIS shapefile.')
parser.add_argument('datafile', help='The path to the features data file.')
args = parser.parse_args()

MAX_DISTANCE = 0.1  # Angular distance, approximately 10km.

print('Loading urban areas...')

urban_areas = {}  # Maps area name to Shapely polygon.

shapefile = ogr.Open(args.shapefile)
layer = shapefile.GetLayer(0)

for i in range(layer.GetFeatureCount()):
    print('Dilating feature {} of {}'.format(i, layer.GetFeatureCount()))
    feature = layer.GetFeature(i)
    name = feature.GetField('NAME')
    geometry = feature.GetGeometryRef()
    outline = shapely.wkt.loads(geometry.ExportToWkt())
    dilated_outline = outline.buffer(MAX_DISTANCE)
    urban_areas[name] = dilated_outline

print('Checking parks...')

with open(args.datafile, 'r') as f:
    for line in f.readlines():
        chunks = line.rstrip().split('|')
        try:
            if chunks[2] == 'Park':
                park_name = chunks[1]
                latitude = float(chunks[9])
                longitude = float(chunks[10])

                point = shapely.geometry.Point(longitude, latitude)

                for urban_name, urban_area in urban_areas.items():
                    if urban_area.contains(point):
                        print(
                            '{} is in or near {}'.format(park_name, urban_name)
                        )
        except IndexError:
            pass
