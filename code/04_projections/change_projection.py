"""
This program will change projections to combine two shapefiles using Universal
Transverse Mercator coordinates and geographic coordinates.
"""
import argparse

from osgeo import ogr, osr

# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser()
parser.add_argument('shapefile', help='The path to the source shapefile.')
parser.add_argument('destination', help='The destination file to be created.')
args = parser.parse_args()

# Define the source and destination projections and a transformation object to
# convert from one to the other.
src_projection = osr.SpatialReference()
src_projection.SetUTM(17)

dst_projection = osr.SpatialReference()
dst_projection.SetWellKnownGeogCS('WGS84')  # Lat/Long

transform = osr.CoordinateTransformation(src_projection, dst_projection)

# Open the source shapefile.
src_file = ogr.Open(args.shapefile)
src_layer = src_file.GetLayer(0)

# Create the destination shapefile and give it the new projection.
driver = ogr.GetDriverByName('ESRI Shapefile')
dst_file = driver.CreateDataSource(args.destination)
dst_layer = dst_file.CreateLayer('layer', dst_projection)

# Reproject each feature in turn.
for i in range(src_layer.GetFeatureCount()):
    feature = src_layer.GetFeature(i)
    geometry = feature.GetGeometryRef()

    new_geometry = geometry.Clone()
    new_geometry.Transform(transform)

    feature = ogr.Feature(dst_layer.GetLayerDefn())
    feature.SetGeometry(new_geometry)
    dst_layer.CreateFeature(feature)
