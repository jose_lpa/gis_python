"""
This script can deal with any supplied projection and datum and, at the same
time, process all the features in the shapefile rather thna just the first.

The script can work therefore with any arbitrary shapefile provided.
"""
import argparse

import osr
import pyproj
from osgeo import ogr


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser(
    description="Example: python border_length.py common-border/border.shp"
)
parser.add_argument('shapefile', help='The path to the shapefile to use.')
args = parser.parse_args()

shapefile = ogr.Open(args.shapefile)
layer = shapefile.GetLayer(0)
spatial_ref = layer.GetSpatialRef()

if spatial_ref is None:
    print("Shapefile has no spatial reference. Using 'WGS84' by default.")
    spatial_ref = osr.SpatialReference()
    spatial_ref.SetWellKnownGeogCS('WGS84')

if spatial_ref.IsProjected():
    # Convert projected coordinates back to lat/long values.
    source_proj = pyproj.Proj(spatial_ref.ExportToProj4())
    destination_proj = pyproj.Proj(proj='longlat', ellps='WGS84', datum='WGS84')


def get_line_segments_from_geometry(geometry):
    segments = []

    if geometry.GetPointCount() > 0:
        segment = []

        for i in range(geometry.GetPointCount()):
            segment.append(geometry.GetPoint_2D(i))

        segments.append(segment)

    for i in range(geometry.GetGeometryCount()):
        subgeometry = geometry.GetGeometryRef(i)
        segments.extend(
            get_line_segments_from_geometry(subgeometry)
        )

    return segments


for i in range(layer.GetFeatureCount()):
    feature = layer.GetFeature(i)

    geometry = feature.GetGeometryRef()
    segments = get_line_segments_from_geometry(geometry)

    # Calculate geodetic distance using pyproj.
    geod = pyproj.Geod(ellps='WGS84')  # Instantiate with a specific datum.

    # Iterate over line segments, calculate distance from one point to the next,
    # and total up all distances to obtain total length of the border.
    total_length = 0.0
    for segment in segments:
        for j in range(len(segment) - 1):
            point_1 = segment[j]
            point_2 = segment[j + 1]

            long_1, lat_1 = point_1
            long_2, lat_2 = point_2

            # Transform coordinates back to 'WGS84'.
            if spatial_ref.IsProjected():
                long_1, lat_1 = pyproj.transform(
                    source_proj,
                    destination_proj,
                    long_1,
                    lat_1
                )
                long_2, lat_2 = pyproj.transform(
                    source_proj,
                    destination_proj,
                    long_2,
                    lat_2
                )

            try:
                angle_1, angle_2, distance = geod.inv(
                    long_1,
                    lat_1,
                    long_2,
                    lat_2
                )
            except ValueError:
                print(
                    'Unable to calculate distance from {:.4f},{:.4f} to '
                    '{:.4f},{:.4f}'.format(long_1, lat_1, long_2, lat_2)
                )

            total_length += distance

    print(
        'Total length of feature {} is {:.2f} km'.format(i, total_length / 1000)
    )
