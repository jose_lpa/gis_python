"""
This script uses a shapefile created with the ``02_common_borders`` script:
``python common_borders.py ../../world_borders_data/TM_WORLD_BORDERS-0.3.shp``

That command will produce a shapefile border.shp (included in ``common-border``
directory for convenience) which we'll make use of in order to calculate the
length of the Thailand-Myanmar border.
"""
import argparse

import pyproj
from osgeo import ogr


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser(
    description="Example: python border_length.py common-border/border.shp"
)
parser.add_argument('shapefile', help='The path to the shapefile to use.')
args = parser.parse_args()

shapefile = ogr.Open(args.shapefile)
layer = shapefile.GetLayer(0)
feature = layer.GetFeature(0)
geometry = feature.GetGeometryRef()


def get_line_segments_from_geometry(geometry):
    segments = []

    if geometry.GetPointCount() > 0:
        segment = []

        for i in range(geometry.GetPointCount()):
            segment.append(geometry.GetPoint_2D(i))

        segments.append(segment)

    for i in range(geometry.GetGeometryCount()):
        subgeometry = geometry.GetGeometryRef(i)
        segments.extend(
            get_line_segments_from_geometry(subgeometry)
        )

    return segments


segments = get_line_segments_from_geometry(geometry)

# Calculate geodetic distance using pyproj.
geod = pyproj.Geod(ellps='WGS84')  # Instantiate with a specific datum.

# Iterate over line segments, calculate distance from one point to the next, and
# total up all distances to obtain total length of the border.
total_length = 0.0
for segment in segments:
    for i in range(len(segment) - 1):
        point_1 = segment[i]
        point_2 = segment[i + 1]

    long_1, lat_1 = point_1
    long_2, lat_2 = point_2

    # Inverse geodetic transformation method. `angle_1` will be the angle from
    # the 1st point to the 2nd measured in decimal degrees, `angle_2` will be
    # the angle from the 2nd point back to the 1st and `distance` will be the
    # great-circle distance between the two points in meters.
    angle_1, angle_2, distance = geod.inv(long_1, lat_1, long_2, lat_2)

    total_length += distance

print('Total border length = {:.2f} km'.format(total_length / 1000))
