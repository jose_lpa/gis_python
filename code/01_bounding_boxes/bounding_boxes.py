import argparse

from osgeo import ogr


# Get the shapefile to use from the shell arguments.
parser = argparse.ArgumentParser()
parser.add_argument('shapefile', help='The path to the shapefile to use.')
args = parser.parse_args()

shapefile = ogr.Open(args.shapefile)
layer = shapefile.GetLayer(0)

countries = []

for i in range(layer.GetFeatureCount()):
    feature = layer.GetFeature(i)
    country_code = feature.GetField('ISO3')
    country_name = feature.GetField('NAME')
    geometry = feature.GetGeometryRef()
    min_lon, max_lon, min_lat, max_lat = geometry.GetEnvelope()

    countries.append(
        (
            country_name,
            country_code,
            min_lat,
            max_lat,
            min_lon,
            max_lon
        )
    )
    countries.sort()


for name, code, min_lat, max_lat, min_lon, max_lon in countries:
    print('{} ({}) latitude: {:.4f}..{:.4f}, longitude: {:.4f}..{:.4f}'.format(
        name, code, min_lat, max_lat, min_lon, max_lon)
    )
